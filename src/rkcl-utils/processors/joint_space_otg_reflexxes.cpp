/**
 * @file joint_space_otg_reflexxes.cpp
 * @author Sonny Tarbouriech (LIRMM)
 * @brief Define a joint space online trajectory generator based on ReReflexxes (a rewrite of the Reflexxes online trajectory generator for a modern C++ API)
 * @date 31-01-2020
 * License: CeCILL
 */
#include <rkcl/processors/joint_space_otg_reflexxes.h>
#include <yaml-cpp/yaml.h>
#include <iostream>

using namespace rkcl;

JointSpaceOTGReflexxes::JointSpaceOTGReflexxes(JointGroupPtr joint_group)
    : result_(rml::ResultValue::Working),
      JointSpaceOTG(joint_group){};

JointSpaceOTGReflexxes::JointSpaceOTGReflexxes(
    Robot& robot,
    const YAML::Node& configuration)
    : result_(rml::ResultValue::Working),
      JointSpaceOTG(robot, configuration)
{
    configure(robot, configuration);
}

JointSpaceOTGReflexxes::~JointSpaceOTGReflexxes() = default;

void JointSpaceOTGReflexxes::configure(Robot& robot, const YAML::Node& configuration)
{
    if (configuration)
    {
        // TODO
    }
}

const rml::ResultValue& JointSpaceOTGReflexxes::result()
{
    return result_;
}

void JointSpaceOTGReflexxes::reset()
{
    JointSpaceOTG::reset();
    if (control_mode_ == ControlMode::Position)
    {
        if (not position_otg_)
        {
            position_otg_ = std::make_unique<rml::PositionOTG>(joint_group_->jointCount(), joint_group_->controlTimeStep());
            if (synchronize_)
                position_otg_->flags.synchronization_behavior = rml::SynchronizationBehavior::PhaseSynchronizationIfPossible;
            else
                position_otg_->flags.synchronization_behavior = rml::SynchronizationBehavior::NoSynchronization;
        }

        for (size_t i = 0; i < joint_group_->jointCount(); ++i)
        {
            if (joint_group_->controlSpace() == JointGroup::ControlSpace::JointSpace && joint_group_->selectionMatrix()->diagonal()(i) > 0)
            {
                position_otg_->input.selection()[i] = true;
            }
            else
            {
                position_otg_->input.selection()[i] = false;
            }
        }
        std::lock_guard<std::mutex> lock(joint_group_->state_mtx_);
        previous_state_position_ = joint_group_->state().position();

        if (joint_group_->goal().position().isApprox(previous_state_position_) and joint_group_->goal().velocity().isApprox(joint_group_->state().velocity()))
            result_ = rml::ResultValue::FinalStateReached;
        else
            result_ = rml::ResultValue::Working;
    }
    else if (control_mode_ == ControlMode::Velocity)
    {
        if (not velocity_otg_)
        {
            velocity_otg_ = std::make_unique<rml::VelocityOTG>(joint_group_->jointCount(), joint_group_->controlTimeStep());
            if (synchronize_)
                velocity_otg_->flags.synchronization_behavior = rml::SynchronizationBehavior::OnlyTimeSynchronization;
            else
                velocity_otg_->flags.synchronization_behavior = rml::SynchronizationBehavior::NoSynchronization;
        }

        for (size_t i = 0; i < joint_group_->jointCount(); ++i)
        {
            if (joint_group_->controlSpace() == JointGroup::ControlSpace::JointSpace && joint_group_->selectionMatrix()->diagonal()(i) > 0)
            {
                velocity_otg_->input.selection()[i] = true;
            }
            else
            {
                velocity_otg_->input.selection()[i] = false;
            }
        }
        if (joint_group_->goal().velocity().isApprox(joint_group_->state().velocity()))
            result_ = rml::ResultValue::FinalStateReached;
        else
            result_ = rml::ResultValue::Working;
    }
    is_reset_ = true;
}

bool JointSpaceOTGReflexxes::process()
{
    if (result_ != rml::ResultValue::FinalStateReached)
    {
        if (control_mode_ == ControlMode::Position)
            return processPositionBasedOTG();
        else if (control_mode_ == ControlMode::Velocity)
            return processVelocityBasedOTG();
    }

    return true;
}

bool JointSpaceOTGReflexxes::processPositionBasedOTG()
{
    bool input_current_state = inputCurrentState();

    std::copy(joint_group_->limits().maxVelocity().value().data(), joint_group_->limits().maxVelocity().value().data() + joint_group_->jointCount(), position_otg_->input.maxVelocity().data());
    std::copy(joint_group_->limits().maxAcceleration().value().data(), joint_group_->limits().maxAcceleration().value().data() + joint_group_->jointCount(), position_otg_->input.maxAcceleration().data());
    std::copy(joint_group_->goal().position().data(), joint_group_->goal().position().data() + joint_group_->jointCount(), position_otg_->input.targetPosition().data());
    std::copy(joint_group_->goal().velocity().data(), joint_group_->goal().velocity().data() + joint_group_->jointCount(), position_otg_->input.targetVelocity().data());

    if (input_current_state)
    {
        std::lock_guard<std::mutex> lock(joint_group_->state_mtx_);
        Eigen::VectorXd estimated_current_position = previous_state_position_ + joint_group_->state().velocity() * joint_group_->controlTimeStep();
        std::copy(estimated_current_position.data(), estimated_current_position.data() + joint_group_->jointCount(), position_otg_->input.currentPosition().data());
        // std::copy(joint_group_->state().position().data(), joint_group_->state().position().data() + joint_group_->jointCount(), position_otg_->input.currentPosition().data());
        std::copy(joint_group_->state().velocity().data(), joint_group_->state().velocity().data() + joint_group_->jointCount(), position_otg_->input.currentVelocity().data());
    }
    else
    {
        position_otg_->input.currentPosition() = position_otg_->output.newPosition();
        position_otg_->input.currentVelocity() = position_otg_->output.newVelocity();
    }

    result_ = (*position_otg_)();

    std::copy(position_otg_->output.newPosition().begin(), position_otg_->output.newPosition().end(), joint_group_->target().position().data());
    std::copy(position_otg_->output.newVelocity().begin(), position_otg_->output.newVelocity().end(), joint_group_->target().velocity().data());

    previous_state_position_ = joint_group_->state().position();

    return true;
}

bool JointSpaceOTGReflexxes::processVelocityBasedOTG()
{
    bool input_current_state = inputCurrentState();
    std::copy(joint_group_->limits().maxAcceleration().value().data(), joint_group_->limits().maxAcceleration().value().data() + joint_group_->jointCount(), velocity_otg_->input.maxAcceleration().data());
    std::copy(joint_group_->goal().velocity().data(), joint_group_->goal().velocity().data() + joint_group_->jointCount(), velocity_otg_->input.targetVelocity().data());

    if (input_current_state)
    {
        std::lock_guard<std::mutex> lock(joint_group_->state_mtx_);
        if (joint_group_->goal().velocity().isApprox(joint_group_->state().velocity()))
        {
            result_ = rml::ResultValue::FinalStateReached;
            return true;
        }
        else
        {
            std::copy(joint_group_->state().position().data(), joint_group_->state().position().data() + joint_group_->jointCount(), velocity_otg_->input.currentPosition().data());
            std::copy(joint_group_->state().velocity().data(), joint_group_->state().velocity().data() + joint_group_->jointCount(), velocity_otg_->input.currentVelocity().data());
        }
    }
    else
    {
        velocity_otg_->input.currentPosition() = velocity_otg_->output.newPosition();
        velocity_otg_->input.currentVelocity() = velocity_otg_->output.newVelocity();
    }

    result_ = (*velocity_otg_)();

    std::copy(velocity_otg_->output.newVelocity().begin(), velocity_otg_->output.newVelocity().end(), joint_group_->target().velocity().data());
    if (input_current_state)
    {
        joint_group_->target().position() = joint_group_->state().position() + joint_group_->target().velocity() * joint_group_->controlTimeStep();
    }
    else
    {
        joint_group_->target().position() = joint_group_->target().position() + joint_group_->target().velocity() * joint_group_->controlTimeStep();
    }

    return true;
}

bool JointSpaceOTGReflexxes::finalStateReached() const
{
    return (result_ == rml::ResultValue::FinalStateReached);
}