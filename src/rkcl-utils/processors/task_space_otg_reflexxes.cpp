/**
 * @file task_space_otg_reflexxes.cpp
 * @author Sonny Tarbouriech (LIRMM)
 * @brief Define a task space online trajectory generator based on ReReflexxes (a rewrite of the Reflexxes online trajectory generator for a modern C++ API)
 * @date 31-01-2020
 * License: CeCILL
 */
#include <rkcl/processors/task_space_otg_reflexxes.h>
#include <yaml-cpp/yaml.h>
#include <iostream>

using namespace rkcl;

TaskSpaceOTGReflexxes::TaskSpaceOTGReflexxes(
    Robot& robot,
    const double& cycle_time)
    : result_(rml::ResultValue::Working),
      TaskSpaceOTG(robot, cycle_time){};

TaskSpaceOTGReflexxes::TaskSpaceOTGReflexxes(
    Robot& robot,
    const double& cycle_time,
    const YAML::Node& configuration)
    : result_(rml::ResultValue::Working),
      TaskSpaceOTG(robot, cycle_time, configuration)
{
    configure(robot, configuration);
}

TaskSpaceOTGReflexxes::~TaskSpaceOTGReflexxes() = default;

void TaskSpaceOTGReflexxes::configure(Robot& robot, const YAML::Node& configuration)
{
    if (configuration)
    {
        // TODO
    }
}

const rml::ResultValue& TaskSpaceOTGReflexxes::result()
{
    return result_;
}

void TaskSpaceOTGReflexxes::reset()
{
    if (not otg_ || (robot_.controlPointCount() != control_point_count_))
    {
        control_point_count_ = robot_.controlPointCount();
        otg_ = std::make_unique<rml::SynchronizedPoseOTG>(control_point_count_, cycle_time_);
        if (not synchronize_)
            otg_->flags.synchronization_behavior = rml::SynchronizationBehavior::NoSynchronization;
    }
    previous_state_pose_.resize(robot_.controlPointCount());
    for (size_t i = 0; i < robot_.controlPointCount(); ++i)
    {
        auto cp = robot_.controlPoint(i);

        auto pos_adm_sel_matrix = cp->selectionMatrix().positionControl().diagonal() + cp->selectionMatrix().admittanceControl().diagonal();
        for (size_t j = 0; j < 6; ++j)
            if (cp->generateTrajectory() && pos_adm_sel_matrix(j) > 0)
                otg_->input[i].selection()[j] = true;
            else
                otg_->input[i].selection()[j] = false;

        previous_state_pose_[i] = cp->state().pose();
    }
    result_ = rml::ResultValue::Working;
    is_reset_ = true;
}

bool TaskSpaceOTGReflexxes::process()
{
    if (not otg_)
        throw std::runtime_error("Task space OTG should be reset first");
    bool input_current_state = inputCurrentState();
    for (size_t i = 0; i < robot_.controlPointCount(); ++i)
    {
        auto cp = robot_.controlPoint(i);
        auto& input = otg_->input.at(i);
        const auto& output = otg_->output.at(i);

        input.maxTwist() = cp->limits().maxVelocity().value();
        input.maxAccelerationCart() = cp->limits().maxAcceleration().value();
        input.targetPose() = cp->goal().pose();
        input.targetTwist() = cp->goal().twist();

        if (input_current_state)
        {
            //TODO : Should be tested on the real robot
            input.currentPose() = estimateCurrentPose(i);
            // input.currentPose() = cp->state.pose;
            input.currentTwist() = cp->state().twist();
        }
        else
        {
            input.currentPose() = output.newPose();
            input.currentTwist() = output.newTwist();
        }
    }

    result_ = (*otg_)();

    for (size_t i = 0; i < robot_.controlPointCount(); ++i)
    {
        auto cp = robot_.controlPoint(i);
        auto& output = otg_->output.at(i);

        if (cp->generateTrajectory())
        {
            controlPointTarget(i).pose() = output.newPose();
            controlPointTarget(i).twist() = output.newTwist();
        }

        previous_state_pose_[i] = cp->state().pose();
    }
    return true;
}

bool TaskSpaceOTGReflexxes::finalStateReached() const
{
    return (result_ == rml::ResultValue::FinalStateReached);
}
