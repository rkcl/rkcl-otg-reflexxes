/**
 * @file otg_reflexxes.h
 * @author Sonny Tarbouriech (LIRMM)
 * @brief Header file for trajectory generators based on reflexxes
 * @date 31-01-2020
 * License: CeCILL
 */

#pragma once

#include <rkcl/processors/task_space_otg_reflexxes.h>
#include <rkcl/processors/joint_space_otg_reflexxes.h>
