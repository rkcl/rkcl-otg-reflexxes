# [](https://gite.lirmm.fr/rkcl/rkcl-otg-reflexxes/compare/v2.0.0...v) (2022-05-13)


### Features

* update dep to rkcl-core ([cfe89d3](https://gite.lirmm.fr/rkcl/rkcl-otg-reflexxes/commits/cfe89d3c127d5d811c2724c66847226f1f9d4301))



# [2.0.0](https://gite.lirmm.fr/rkcl/rkcl-otg-reflexxes/compare/v1.1.0...v2.0.0) (2021-10-01)


### Bug Fixes

* update dep to rereflexxes ([a76398f](https://gite.lirmm.fr/rkcl/rkcl-otg-reflexxes/commits/a76398fbf9d08f5d5d54462712888eab7637b894))
* use accessor of control point target in task space otg ([d0bf9d0](https://gite.lirmm.fr/rkcl/rkcl-otg-reflexxes/commits/d0bf9d04ba290411291424e806be5906248c4492))
* use rereflexxes v0.2.5 ([dad057f](https://gite.lirmm.fr/rkcl/rkcl-otg-reflexxes/commits/dad057fe8bb21934d64fa13929e04a0b1230c7f4))
* use virtual destructors ([f1e86f3](https://gite.lirmm.fr/rkcl/rkcl-otg-reflexxes/commits/f1e86f38b490a15cddbbacb21883de8afa660984))


### Features

* call reset method from base class in js otg ([a1ad946](https://gite.lirmm.fr/rkcl/rkcl-otg-reflexxes/commits/a1ad94620a7b6eb31d4d10da9699cbd873bc0993))
* use conventional commits ([496c3e1](https://gite.lirmm.fr/rkcl/rkcl-otg-reflexxes/commits/496c3e1ad0fbd7549a53bffc6c3c08385c8536a3))



## [1.0.2](https://gite.lirmm.fr/rkcl/rkcl-otg-reflexxes/compare/v1.0.1...v1.0.2) (2020-10-02)



## 1.0.1 (2020-02-20)



